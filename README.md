# gitrc: Git Run Commands

## Introduction

Welcome to the `gitrc` repository. This repository contains a set of shortcut aliases and custom functions for Git, aimed at streamlining your Git workflow. Instead of typing out lengthy Git commands, you can use these convenient shorthand versions.

For example, instead of typing `git status`, you can simply type `gs`. This might not seem like much, but when you're working in the terminal all day, these shortcuts can save a lot of time!

## Usage

To use these shortcuts:

1. Clone this repository.
2. Source the `.gitrc` file in your shell's configuration file (e.g., `.bashrc` or `.zshrc`):

   `source /path/to/.gitrc`

3. Restart your terminal or run the following command to activate the aliases and functions:

   `source ~/.bashrc`

   Or, for other shells:

   `source ~/.zshrc`

## Contributing

Contributions are welcome! If you have a useful Git shortcut or function that isn't in this repository, or if you have improvements to suggest, please:

1. Fork the repository.
2. Make your changes.
3. Submit a pull request with a clear description of your changes.

Please ensure that your additions don't conflict with existing aliases.

## A Word of Caution

Before using any new aliases or functions, make sure you understand what they do. Misusing Git commands can result in data loss, especially commands that affect the commit history or branch state. Always ensure you're in the correct repository and branch before executing commands, and make regular backups of your work.

A [conflict checker](conflict-checker.sh) is provided to detect whether conflicts exists between your existing aliases, functions, and executables found in your PATH, and the aliases in this repository. To run the conflict checker, run the following command:

`source conflict-checker.sh`

## Key Aliases

Here are some of the primary aliases provided by this `.gitrc` file:

- `ga`: Add changes to the index
- `gb`: List, create, or delete branches
- `gc`: Record changes to the repository
- `gd`: Show changes between commits, commit, and working tree, etc.
- `gf`: Download objects and refs from a remote repository
- `glog`: Show the commit logs
- `gs`: Show the working tree status
- `gsh`: Show information about a commit
- `gst`: Stash changes in a dirty working directory
- And many more!

For a full list and their descriptions, check the `.gitrc` file.

## Feedback

If you encounter any issues or have feedback, please open an issue on the repository's GitHub page. Your insights will help improve this tool for everyone!

Thank you for using `gitrc`, and happy coding!
