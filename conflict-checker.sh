#!/bin/bash

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    echo "Error: The script is being executed. Please source it instead. Otherwise your aliases and functions will not be checked correctly."
    exit 1
fi

GITRC_FILE=".gitrc"

ALIASES_IN_GITRC_FILE=$(grep -E '^alias ' "$GITRC_FILE" | awk '{print $2}' | cut -d'=' -f1)
FUNCTIONS_IN_GITRC_FILE=$(grep -E '^function ' "$GITRC_FILE" | awk '{print $2}' | cut -d'(' -f1)

warnings_found=0

for alias_name in $ALIASES_IN_GITRC_FILE; do
    if command -v "$alias_name" &> /dev/null; then
        echo "Warning: Alias $alias_name conflicts with an existing command in the PATH!"
        warnings_found=1
    fi
done

for func_name in $FUNCTIONS_IN_GITRC_FILE; do
    if command -v "$func_name" &> /dev/null; then
        echo "Warning: Function $func_name conflicts with an existing command in the PATH!"
        warnings_found=1
    fi
done

if [[ $warnings_found -eq 0 ]]; then
    echo "No conflicts found."
fi
